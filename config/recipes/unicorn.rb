set_default(:unicorn_user) { user }
set_default(:unicorn_pid) { "#{current_path}/tmp/pids/unicorn.pid" }
set_default(:unicorn_config) { "#{shared_path}/config/unicorn.rb" }
set_default(:unicorn_log) { "#{shared_path}/log/unicorn.log" }
set_default(:unicorn_workers, 4)

namespace :unicorn do
  desc "Setup Unicorn initializer and app configuration"
  task :setup, :roles => :web do
    run "mkdir -p #{shared_path}/config"
    template "unicorn.rb.erb", unicorn_config
    template "unicorn_init.erb", "/tmp/unicorn_init"
    run "chmod +x /tmp/unicorn_init"
    run "#{sudo} mv /tmp/unicorn_init /etc/init.d/unicorn_#{application}"
    run "#{sudo} update-rc.d -f unicorn_#{application} defaults"
  end
  after "deploy:setup", "unicorn:setup"

  %w[start stop].each do |command|
    desc "#{command} unicorn"
    task command, :roles => :web do
      run "service unicorn_#{application} #{command}"
    end
    after "deploy:#{command}", "unicorn:#{command}"
  end

  desc "restart unicorn"
  task :restart, :roles => :web do
    run "service unicorn_#{application} stop"
    run "service unicorn_#{application} start"
  end
  after "deploy:restart", "unicorn:restart"

  desc "Get output of unicorn log file"
  task :tail do
    stream "tail -n 300 -f #{unicorn_log}"
  end
end
