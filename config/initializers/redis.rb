# Initiates redis connection.
# If Redis is runnin on a separate instance, use Redis.new(host: 'localhost', port: 6379) - Replace host with instance endpoint
$redis = Redis.new(host: ENV["REDIS_HOST"], port: 6379)