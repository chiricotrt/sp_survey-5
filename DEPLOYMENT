DEPLOYMENT WITH CAPISTRANO STEPS
=================================

[Tip Link: https://www.digitalocean.com/community/tutorials/deploying-a-rails-app-on-ubuntu-14-04-with-capistrano-nginx-and-puma]

1) Create a non-root user named deploy with sudo privileges
  [deploy : Wh!mpoll17]
  sudo adduser deploy
  sudo gpasswd -a deploy sudo
  vi .ssh/authorized_keys (add public key of dev machine)
  chmod 600 .ssh/authorized_keys

2) Install Server (Nginx)
  sudo apt-get update
  sudo apt-get install curl git-core nginx -y

3) Install Database (MySQL)
  [root : Db4Wh!m17]
  sudo apt-get install mysql-server
  mysql -u root -p

4) Install RVM and Ruby
  gpg --keyserver hkp://keys.gnupg.net --recv-keys 409B6B1796C275462A1703113804BB82D39DC0E3

  curl -sSL https://get.rvm.io | bash -s stable
  source ~/.rvm/scripts/rvm
  rvm requirements

  rvm install 2.2.5
  rvm use 2.2.5 --default

5) Install Rails and Bundler
  gem install rails -v '4.2.7.1' -V --no-ri --no-rdoc
  gem install bundler -V --no-ri --no-rdoc

6) Set up SSH keys (between server and source repo)
  ssh -T git@bitbucket.org
  (add .ssh/id_rsa.pub into bitbucket setttings)

7) Enable ports in AWS
  sudo ufw allow ssh
  sudo ufw allow 80/tcp
  sudo ufw allow 443/tcp

  sudo ufw show added
  sudo ufw enable

8) Set up time and NTP
  sudo dpkg-reconfigure tzdata

  sudo apt-get update
  sudo apt-get install ntp

9) Set up SWAP
  sudo fallocate -l 1G /swapfile
  sudo chmod 600 /swapfile
  sudo mkswap /swapfile
  sudo swapon /swapfile

  # to enable swap on reboot
  sudo sh -c 'echo "/swapfile none swap sw 0 0" >> /etc/fstab'

10) Set up Capistrano
  group :development do
    gem 'capistrano',         require: false
    gem 'capistrano-rvm',     require: false
    gem 'capistrano-rails',   require: false
    gem 'capistrano-bundler', require: false
    gem 'capistrano3-puma',   require: false
  end

  gem 'puma'

11) Run capistrano to deploy
  cap production deploy:initial (first time)

  #FIXES
    sudo apt-get install libmysqlclient-dev (mysql gem error)

    cap production setup (database.yml issue)

    RAILS_ENV=production bundle exec rake db:setup [in the SERVER!]

  cap production deploy (normal deployment)
    cap production puma:restart

12) Symlink the nginx.conf to the sites-enabled directory
  sudo rm /etc/nginx/sites-enabled/default
  sudo ln -nfs "/home/deploy/apps/survey_mlb/current/config/nginx.conf" "/etc/nginx/sites-enabled/survey_mlb"

  sudo service nginx restart

13) Enable SSL for website (https://certbot.eff.org/#ubuntuxenial-nginx)
  sudo apt-get update
  sudo apt-get install software-properties-common
  sudo add-apt-repository ppa:certbot/certbot
  sudo apt-get update
  sudo apt-get install python-certbot-nginx

  sudo certbot --nginx

  # FIXES
    Add a line to nginx config file
    proxy_set_header X-Forwarded-Proto https;
