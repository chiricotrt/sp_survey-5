class ModifyPickedPlansToResponses < ActiveRecord::Migration
  def change
    remove_column :sp_experiments, :picked_plans, :text
    add_column :sp_experiments, :responses, :text
  end
end
